# README #

This solution has been built using Visual Studio 2015 Ultimate.

### What is this repository for? ###

* Software Development Test for FM Outsource

### How do I get set up? ###

* Uses NuGet and Visual Studio 2015
* EF uses Code First so will create any required tables when first ran
* The database is located in the App_Data folder, so make sure the Web.Config and Test App.Config point to your file path location


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin