﻿namespace SocialMediaApplication.Models.Authorisation
{
    public class TwitterAuthorisationModel : AAuthorisationModel
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
    }
}
