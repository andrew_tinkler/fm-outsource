﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Autofac;
using SocialMediaApplication.Application;
using SocialMediaApplication.Business.SocialMediaStrategies;
using SocialMediaApplication.Common.Extensions;
using SocialMediaApplication.Models.Data;

namespace SocialMediaApplication.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var container = AutofacModule.BuildContainer();
            var twitterSocialConnection = container.Resolve<ASocialConnection>(new NamedParameter("strategyType", StrategyTypes.Twitter.Description()));
            var instagramSocialConnection = container.Resolve<ASocialConnection>(new NamedParameter("strategyType", StrategyTypes.Instagram.Description()));
            var facebookSocialConnection = container.Resolve<ASocialConnection>(new NamedParameter("strategyType", StrategyTypes.Facebook.Description()));

            var twitterResults = twitterSocialConnection.Execute(20, "twitterapi");

            return View(twitterResults.Cast<TwitterDataModel>().ToList());
        }
    }
}
