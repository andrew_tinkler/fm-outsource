﻿using System.Web.Http;
using System.Web.Http.Dispatcher;

namespace SocialMediaApplication.Application.Setup
{
    public static class WebApiConfig
    {
        //public static void Register(HttpConfiguration config)
        //{
        //    config.Routes.MapHttpRoute(
        //        name: "DefaultApi",
        //        routeTemplate: "api/{controller}/{id}",
        //        defaults: new { id = RouteParameter.Optional }
        //    );
        //} 

        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new {id = RouteParameter.Optional}
                );

            //config.Routes.MapHttpRoute(
            //    name: "TwitterUserTimeLine",
            //    routeTemplate: "api/Twitter/UserTimeLine/{count}/{screenName}",
            //    defaults: new {count = 10, screenName = "twitterapi"}
            //    );

            config.EnsureInitialized();
        }
    }
}