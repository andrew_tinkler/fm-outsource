﻿using System.Diagnostics.Eventing.Reader;
using Autofac;
using Autofac.Integration.Mvc;
using SocialMediaApplication.Business.SocialMediaStrategies;
using SocialMediaApplication.Common.Extensions;
using SocialMediaApplication.Common.Serialiser;
using SocialMediaApplication.Data.Context;
using SocialMediaApplication.Data.Interfaces;
using SocialMediaApplication.Data.Repositories;
using Module = Autofac.Module;

namespace SocialMediaApplication.Application
{
    public class AutofacModule : Module
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static IContainer BuildContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule<AutofacModule>();
            return builder.Build();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterControllers(typeof (MvcApplication).Assembly);

            builder.Register(c => new JsonSerializer()).As<ISerializer>().InstancePerLifetimeScope();
            builder.Register(c => new DataContext()).As<ADataContext>().InstancePerLifetimeScope();
            builder.Register(c => new DbRepository(c.Resolve<ADataContext>(new NamedParameter("commandTimeout", 1800)))).As<IDbRepository>().InstancePerLifetimeScope();

            builder.Register<ASocialConnection>(
                (c, p) =>
                {
                    var strategyType = p.Named<string>("strategyType");

                    if (strategyType.Equals(StrategyTypes.Twitter.Description()))
                    {
                        return new Twitter(c.Resolve<ISerializer>(), c.Resolve<IDbRepository>());
                    }

                    if (strategyType.Equals(StrategyTypes.Facebook.Description()))
                    {
                        return new Facebook(c.Resolve<ISerializer>(), c.Resolve<IDbRepository>());
                    }

                    if (strategyType.Equals(StrategyTypes.Instagram.Description()))
                    {
                        return new Instagram(c.Resolve<ISerializer>(), c.Resolve<IDbRepository>());
                    }

                    return new NullStrategy();
                });
        }
    }
}