﻿using System;
using System.Web.Http;
using Autofac;
using SocialMediaApplication.Application;
using SocialMediaApplication.Business.SocialMediaStrategies;

namespace SocialMediaApplication.ApiControllers
{
    public class SocialMediaController : ApiController
    {
        [HttpGet]
        [Route("api/SocialMedia/GetDefaultData/{count}/{screenName}/{strategyType}")]
        public IHttpActionResult GetDefaultData(int count, string screenName, string strategyType)
        {
            try
            {
                var container = AutofacModule.BuildContainer();
                var socialConnection = container.Resolve<ASocialConnection>(new NamedParameter("strategyType", strategyType));

                var results = socialConnection.Execute(count, screenName);
                return Ok(results);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}