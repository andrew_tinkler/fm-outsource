CREATE TABLE [dbo].[TwitterAccess] (
    [TwitterAccessId] INT           NOT NULL IDENTITY(1,1),
    [ScreenName]      VARCHAR (50)  NOT NULL,
    [Count]           INT           NOT NULL,
    [DateRequest]     DATETIME2 (7) DEFAULT (getdate()) NOT NULL,
    PRIMARY KEY CLUSTERED ([TwitterAccessId] ASC)
);

