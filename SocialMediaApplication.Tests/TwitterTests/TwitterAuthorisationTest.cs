﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SocialMediaApplication.Business.SocialMediaStrategies;
using SocialMediaApplication.Common.Serialiser;
using SocialMediaApplication.Common.SystemConfig;
using SocialMediaApplication.Models.Authorisation;
using SocialMediaApplication.Models.Data;

namespace SocialMediaApplication.Tests.TwitterTests
{
    [TestClass]
    public class TwitterAuthorisationTest
    {
        [TestMethod]
        public void TestAuthorisationValue()
        {
            ASocialConnection twitterConnection = new Twitter(new JsonSerializer(), null);

            var authorisationInformation = twitterConnection.GetAuthorisation(Settings.TwitterConsumerKey, Settings.TwitterConsumerSecret);

            var twitterAuthorisationModel = (TwitterAuthorisationModel)authorisationInformation;

            Assert.IsTrue(!string.IsNullOrEmpty(twitterAuthorisationModel.access_token));
            Assert.IsTrue(!string.IsNullOrEmpty(twitterAuthorisationModel.token_type));
        }

        [TestMethod]
        public void TestExecute()
        {
            ASocialConnection twitterConnection = new Twitter(new JsonSerializer(), null);

            var results = twitterConnection.Execute(10, "twitterapi");

            foreach (var result in results)
            {
                var twitterModel = (TwitterDataModel) result;
                Console.WriteLine($"Created At: {twitterModel.created_at}");
                Console.WriteLine($"Text: {twitterModel.text}");

                Assert.IsTrue(!string.IsNullOrEmpty(twitterModel.created_at));
                Assert.IsTrue(!string.IsNullOrEmpty(twitterModel.text));
            }
        }
    }
}