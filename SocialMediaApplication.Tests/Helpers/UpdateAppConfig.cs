﻿using System.Configuration;

namespace SocialMediaApplication.Tests.Helpers
{
    public static class UpdateAppConfig
    {
        /// <summary>
        /// Set the appSettings to whatever are required to aid in testing
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static void UpdateAppSettingValue(string key, string value)
        {
            var configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            configuration.AppSettings.Settings[key].Value = value;
            configuration.Save();

            ConfigurationManager.RefreshSection("appSettings");
        }

        /// <summary>
        /// Set the appSettings to whatever are required to aid in testing
        /// </summary>
        /// <param name="oldKey"></param>
        /// <param name="newKey"></param>
        public static void UpdateAppSettingKey(string oldKey, string newKey)
        {
            var configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            configuration.AppSettings.Settings.Remove(oldKey);

            configuration.AppSettings.Settings.Add(newKey, newKey);
            configuration.Save();

            ConfigurationManager.RefreshSection("appSettings");
        }
    }
}
