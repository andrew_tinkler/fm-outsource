﻿using System;
using System.ComponentModel;

namespace SocialMediaApplication.Common.Extensions
{
    public static class EnumExtensions
    {

        /// <summary>
        /// Returns the description attribute of the enum (if there is one, otherwise the enum is returned)
        /// </summary>
        /// <param name="value">the enum</param>
        /// <returns>string</returns>
        public static string Description(this Enum value)
        {
            var enumInfo = value.GetType().GetField(value.ToString());
            var enumAttributes = (DescriptionAttribute[]) enumInfo.GetCustomAttributes(typeof (DescriptionAttribute), false);

            return enumAttributes.Length > 0 ? enumAttributes[0].Description : value.ToString();
        }
    }
}