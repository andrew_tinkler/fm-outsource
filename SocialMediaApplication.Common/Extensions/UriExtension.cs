﻿using System;

namespace SocialMediaApplication.Common.Extensions
{
    public static class UriExtension
    {
        public static Uri ToUri(this string uriString)
        {
            if (!string.IsNullOrEmpty(uriString))
            {
                return new Uri(uriString);
            }

            return null;
        }
    }
}
