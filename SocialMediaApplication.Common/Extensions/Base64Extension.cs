﻿using System;
using System.Text;

namespace SocialMediaApplication.Common.Extensions
{
    public static class Base64Extension
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="toEncode"></param>
        /// <returns></returns>
        public static string Base64Encode(this string toEncode)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(toEncode);
            return Convert.ToBase64String(plainTextBytes);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="toDecode"></param>
        /// <returns></returns>
        public static string Base64Decode(this string toDecode)
        {
            var base64EncodedBytes = Convert.FromBase64String(toDecode);
            return Encoding.UTF8.GetString(base64EncodedBytes);
        }
    }
}
