﻿using System;

namespace SocialMediaApplication.Common.Exceptions
{
    public class AppSettingException : Exception
    {
        public AppSettingException()
        {
        }

        public AppSettingException(string message)
            : base(message)
        {
        }

        public AppSettingException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
