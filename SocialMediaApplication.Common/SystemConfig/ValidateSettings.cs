﻿using SocialMediaApplication.Common.Exceptions;

namespace SocialMediaApplication.Common.SystemConfig
{
    public static class ValidateSetting
    {
        /// <summary>
        /// Ensure a value has been retrieved from the AppConfig - throw an Exception if nothing found
        /// </summary>
        /// <param name="appValue"></param>
        /// <param name="appSettingName"></param>
        public static void String(string appValue, string appSettingName)
        {
            if (string.IsNullOrEmpty(appValue))
            {
                ThrowExceptionAppSettingNotFound(appSettingName);
            }
        }

        /// <summary>
        /// Ensure a value has been retrieved from the AppConfig - throw an Exception if nothing found
        /// Also ensure can be converted to INT
        /// </summary>
        /// <param name="appValue"></param>
        /// <param name="appSettingName"></param>
        /// <returns></returns>
        public static int Int(string appValue, string appSettingName)
        {
            String(appValue, appSettingName);

            int parsedAppValue;

            // If appValue can't be parsed throw an exception
            if (!int.TryParse(appValue, out parsedAppValue))
            {
                ThrowExceptionAppSettingConversionFailed(appValue, appSettingName);
            }

            // otherwise return parsed value
            return parsedAppValue;
        }

        /// <summary>
        /// Ensure a value has been retrieved from the AppConfig - throw an Exception if nothing found
        /// Also ensure can be converted to BOOL
        /// </summary>
        /// <param name="appValue"></param>
        /// <param name="appSettingName"></param>
        /// <returns></returns>
        public static bool Bool(string appValue, string appSettingName)
        {
            String(appValue, appSettingName);

            bool parsedAppValue;

            // If appValue can't be parsed throw an exception
            if (!bool.TryParse(appValue, out parsedAppValue))
            {
                ThrowExceptionAppSettingConversionFailed(appValue, appSettingName);
            }

            // otherwise return parsed value
            return parsedAppValue;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="appSettingName"></param>
        private static void ThrowExceptionAppSettingNotFound(string appSettingName)
        {
            throw new AppSettingException($"Unable to find a Config setting for appSetting {appSettingName}.");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="appValue"></param>
        /// <param name="appSettingName"></param>
        private static void ThrowExceptionAppSettingConversionFailed(string appValue, string appSettingName)
        {
            throw new AppSettingException($"Unable to convert appSetting {appSettingName} - value {appValue}.");
        }
    }
}
