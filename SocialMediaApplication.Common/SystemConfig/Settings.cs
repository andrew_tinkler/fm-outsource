﻿using System.Collections;
using System.Configuration;

namespace SocialMediaApplication.Common.SystemConfig
{
    public static class Settings
    {
        /// <summary>
        /// 
        /// </summary>
        public static Hashtable SocialMediaStrategies
        {
            get
            {
                return ConfigurationManager.GetSection(SocialMediaStrategiesSectionName) as Hashtable; 
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static string SocialMediaStrategiesSectionName
        {
            get
            {
                var socialMediaStrategiesSection = ConfigurationManager.AppSettings["SocialMediaStrategiesSectionName"]; 

                ValidateSetting.String(socialMediaStrategiesSection, "SocialMediaStrategiesSectionName");

                return socialMediaStrategiesSection;
            }
        }

        #region Social Media Keys

        /// <summary>
        /// 
        /// </summary>
        public static string TwitterAuthorisationEndpoint
        {
            get
            {
                var twitterAuthorisationEndpoint = ConfigurationManager.AppSettings["TwitterAuthorisationEndpoint"];

                ValidateSetting.String(twitterAuthorisationEndpoint, "TwitterAuthorisationEndpoint");

                return twitterAuthorisationEndpoint;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static string TwitterTimelineEndpoint
        {
            get
            {
                var twitterTimelineEndpoint = ConfigurationManager.AppSettings["TwitterTimelineEndpoint"];

                ValidateSetting.String(twitterTimelineEndpoint, "TwitterTimelineEndpoint");

                return twitterTimelineEndpoint;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static string TwitterConsumerKey
        {
            get
            {
                var twitterConsumerKey = ConfigurationManager.AppSettings["TwitterConsumerKey"];

                ValidateSetting.String(twitterConsumerKey, "TwitterConsumerKey");

                return twitterConsumerKey;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static string TwitterConsumerSecret
        {
            get
            {
                var twitterConsumerSecret = ConfigurationManager.AppSettings["TwitterConsumerSecret"];

                ValidateSetting.String(twitterConsumerSecret, "TwitterConsumerSecret");

                return twitterConsumerSecret;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static string FacebookConsumerKey
        {
            get
            {
                var facebookConsumerKey = ConfigurationManager.AppSettings["FacebookConsumerKey"];

                ValidateSetting.String(facebookConsumerKey, "FacebookConsumerKey");

                return facebookConsumerKey;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static string FacebookConsumerSecret
        {
            get
            {
                var facebookConsumerSecret = ConfigurationManager.AppSettings["FacebookConsumerSecret"];

                ValidateSetting.String(facebookConsumerSecret, "FacebookConsumerSecret");

                return facebookConsumerSecret;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static string InstagramConsumerKey
        {
            get
            {
                var instagramConsumerKey = ConfigurationManager.AppSettings["InstagramConsumerKey"];

                ValidateSetting.String(instagramConsumerKey, "InstagramConsumerKey");

                return instagramConsumerKey;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static string InstagramConsumerSecret
        {
            get
            {
                var instagramConsumerSecret = ConfigurationManager.AppSettings["InstagramConsumerSecret"];

                ValidateSetting.String(instagramConsumerSecret, "InstagramConsumerSecret");

                return instagramConsumerSecret;
            }
        }

        #endregion  
    }
}
