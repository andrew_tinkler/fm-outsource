﻿using Newtonsoft.Json;

namespace SocialMediaApplication.Common.Serialiser
{
    public class JsonSerializer : ISerializer
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="toSerialize"></param>
        /// <returns></returns>
        public string Serialize<T>(T toSerialize)
        {
            var serializedObject = JsonConvert.SerializeObject(toSerialize, Formatting.Indented);
            return serializedObject;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="toDeserialize"></param>
        /// <returns></returns>
        public T Deserialize<T>(string toDeserialize)
        {
            var deserializedObject = JsonConvert.DeserializeObject<T>(toDeserialize);
            return deserializedObject;
        }
    }
}
