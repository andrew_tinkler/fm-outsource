﻿namespace SocialMediaApplication.Common.Serialiser
{
    public interface ISerializer
    {
        T Deserialize<T>(string toDeserialize);

        string Serialize<T>(T toSerialize);
    }
}
