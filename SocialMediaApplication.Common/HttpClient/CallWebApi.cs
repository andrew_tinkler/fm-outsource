﻿using System;
using System.ComponentModel;
using System.IO;
using System.IO.Compression;
using System.Net;
using SocialMediaApplication.Common.Extensions;

namespace SocialMediaApplication.Common.HttpClient
{
    public enum MethodType
    {
        [Description("GET")] Get,
        [Description("POST")] Post,
        [Description("PUT")] Put,
        [Description("DELETE")] Delete,
    }

    /// <summary>
    /// 
    /// </summary>
    public class CallWebApi
    {
        private readonly WebHeaderCollection _headerCollection;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="headerCollection"></param>
        public CallWebApi(WebHeaderCollection headerCollection)
        {
            _headerCollection = headerCollection;
        }

        # region Method to send to Api

        /// <summary>
        /// POST json message to endpoint - string contains json
        /// </summary>
        /// <param name="webApiUri"></param>
        /// <param name="jsonRequest"></param>
        /// <returns></returns>
        public string SendToWebApiPost(Uri webApiUri, string jsonRequest)
        {
            using (var client = new WebClient())
            {
                foreach (var headerKey in _headerCollection.AllKeys)
                {
                    client.Headers[headerKey] = _headerCollection[headerKey];
                }

                var response = client.UploadString(webApiUri, MethodType.Post.Description(), jsonRequest);
                return response;
            }
        }

        /// <summary>
        /// GET data from supplied endpoint
        /// </summary>
        /// <param name="webApiUri"></param>
        /// <returns></returns>
        public string SendToWebApiGet(Uri webApiUri)
        {
            using (var client = new WebClient())
            {
                foreach (var headerKey in _headerCollection.AllKeys)
                {
                    client.Headers[headerKey] = _headerCollection[headerKey];
                }

                var responseStream = new GZipStream(client.OpenRead(webApiUri), CompressionMode.Decompress);
                var reader = new StreamReader(responseStream);
                var response = reader.ReadToEnd();
                
                return response;
            }
        }

        #endregion
    }
}