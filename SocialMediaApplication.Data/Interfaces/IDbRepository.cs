﻿using System.Collections.Generic;
using SocialMediaApplication.Data.Context;
using SocialMediaApplication.Data.Repositories;

namespace SocialMediaApplication.Data.Interfaces
{
    public interface IDbRepository
    {
        ADataContext Context { get; }

        TwitterAccessRepository TwitterAccessRepository { get; }

        string ServerName { get; }

        string DatabaseName { get; }

        int? CommandTimeout { get; }

        List<T> SqlQueryList<T>(string query, params object[] parameters);

        T SqlQueryFirst<T>(string query, params object[] parameters);

        T SqlQuerySingle<T>(string query, params object[] parameters);

        int SqlCommand(string query, params object[] parameters);

        int SqlCommandNoTransaction(string query, params object[] parameters);

        void Commit();
        void Dispose();
    }
}
