﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SocialMediaApplication.Data.Models
{
    public class TwitterAccess
    {
        [Required]
        public int TwitterAccessId { get; set; }

        [Required]
        public string ScreenName { get; set; }

        [Required]
        public int Count { get; set; }

        public DateTime DateCreated { get; set; }
    }
}
