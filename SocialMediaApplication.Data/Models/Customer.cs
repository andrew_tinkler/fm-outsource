﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SocialMediaApplication.Data.Models
{
    public class Customer
    {
        public Customer()
        {
            Addresses = new HashSet<Address>();
        }

        [Required]
        public int CustomerId { get; set; }

        [Required]
        public string Gender { get; set; }

        [Required]
        public DateTime DateOfBirth { get; set; }

        public HashSet<Address> Addresses { get; set; }
    }
}
