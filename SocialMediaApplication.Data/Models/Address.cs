﻿using System.ComponentModel.DataAnnotations;

namespace SocialMediaApplication.Data.Models
{
    public class Address
    {
        [Required]
        public int AddressId { get; set; }
        public string AddressLabel { get; set; }
        public string AddressType { get; set; }
        public string CompanyName { get; set; }
        public string BuildingName { get; set; }
        public string BuildingNumber { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        [Required]
        public string Street { get; set; }
        public string Locality { get; set; }
        [Required]
        public string Town { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        public string PostCode { get; set; }

        [Required]
        public virtual Customer Customer { get; set; }
    }
}
