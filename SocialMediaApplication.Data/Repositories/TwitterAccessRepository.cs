﻿using System;
using SocialMediaApplication.Data.Context;
using SocialMediaApplication.Data.Models;

namespace SocialMediaApplication.Data.Repositories
{
    public class TwitterAccessRepository : RepositoryBase<TwitterAccess>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public TwitterAccessRepository(ADataContext context) : base(context)
        {
            if (Context == null)
                throw new ArgumentNullException(nameof(context));
        }
    }
}
