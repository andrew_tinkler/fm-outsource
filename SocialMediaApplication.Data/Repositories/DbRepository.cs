﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using SocialMediaApplication.Common.Exceptions;
using SocialMediaApplication.Data.Context;
using SocialMediaApplication.Data.Interfaces;

namespace SocialMediaApplication.Data.Repositories
{
    public class DbRepository : IDbRepository
    {
        private readonly ADataContext _context;
        private TwitterAccessRepository _twitterRepository;

        /// <summary>
        /// 
        /// </summary>
        public DbRepository(ADataContext dataContext)
        {
            _context = dataContext;
            InitRepository();
        }

        /// <summary>
        /// 
        /// </summary>
        private void InitRepository()
        {
            //Fix a bug introducted in EF6 where for some stupid reason only known to Microsoft,
            //a reference to EntityFramework is required in the host project
            //One way of fixing this is to add the EF NuGet package to the host project or
            //add a reference to EntityFramework.SqlServer.dll in the host project
            //That is BAD because the host project is designed to have has no dependancy on EF
            //and it should not even need to know about EF!!
            //This code found on a blog post and simply ensures that the dll is copied into the host bin folder
            //to fix the EF6 bug without the need to add a pointless reference to the host project
            EnsureStaticReference<SqlProviderServices>();

            //Now lets check the connection as there is no point continuing if a connection cannot be made
            try
            {
                _context.Database.Connection.Open();
                _context.Database.Connection.Close();
            }
            catch (Exception ex)
            {
                string message =
                    $"The Connection to Server: '{_context.Database.Connection.DataSource}' Database: '{_context.Database.Connection.Database}' could not be opened";
                throw new DatabaseConnectionException(message, ex);
            }
        }


        /// <summary>
        /// 
        /// </summary>
        public ADataContext Context
        {
            get { return _context; }
        }
        
        public TwitterAccessRepository TwitterAccessRepository
        {
            get
            {
                return _twitterRepository ?? (_twitterRepository = new TwitterAccessRepository(_context));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public virtual List<T> SqlQueryList<T>(string query, params object[] parameters)
        {
            return _context.Database.SqlQuery<T>(query, parameters).ToList<T>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public virtual T SqlQueryFirst<T>(string query, params object[] parameters)
        {
            return _context.Database.SqlQuery<T>(query, parameters).FirstOrDefault<T>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public virtual T SqlQuerySingle<T>(string query, params object[] parameters)
        {
            return _context.Database.SqlQuery<T>(query, parameters).SingleOrDefault<T>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public int SqlCommand(string query, params object[] parameters)
        {
            return _context.Database.ExecuteSqlCommand(query, parameters);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public int SqlCommandNoTransaction(string query, params object[] parameters)
        {
            return _context.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, query, parameters);
        }

        /// <summary>
        /// 
        /// </summary>
        public void Commit()
        {
            _context.SaveChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        public string ServerName
        {
            get { return _context.Database.Connection.DataSource; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string DatabaseName
        {
            get { return _context.Database.Connection.Database; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int? CommandTimeout
        {
            get { return _context.Database.CommandTimeout; }
        }

        private bool _disposed;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }

            _disposed = true;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        private static void EnsureStaticReference<T>()
        {
            var dummy = typeof(T);

            if (dummy == null)
                throw new Exception("This code is used to ensure that the compiler will include assembly");
        }
    }
}