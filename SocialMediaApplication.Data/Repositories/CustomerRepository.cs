﻿using System;
using System.Linq;
using SocialMediaApplication.Data.Context;
using SocialMediaApplication.Data.Models;

namespace SocialMediaApplication.Data.Repositories
{
    public class CustomerRepository : RepositoryBase<Customer>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public CustomerRepository(ADataContext context) : base(context)
        {
            if (Context == null) 
                throw new ArgumentNullException(nameof(context));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool ExistsById(int id)
        {
            return Queryable.Any<Customer>(DbSet, item => item.CustomerId == id);
        }
    }
}