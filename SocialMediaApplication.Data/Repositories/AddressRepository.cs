﻿using System;
using System.Linq;
using SocialMediaApplication.Data.Context;
using SocialMediaApplication.Data.Models;

namespace SocialMediaApplication.Data.Repositories
{
    public class AddressRepository : RepositoryBase<Address>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public AddressRepository(ADataContext context)
            : base(context)
        {
            if (Context == null)
                throw new ArgumentNullException(nameof(context));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool ExistsById(int id)
        {
            return Queryable.Any<Address>(DbSet, item => item.AddressId == id);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="addressLabel"></param>
        /// <returns></returns>
        public bool ExistsByLabel(string addressLabel)
        {
            return Queryable.Any<Address>(DbSet, item => item.AddressLabel == addressLabel);
        }
    }
}