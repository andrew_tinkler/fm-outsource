﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using SocialMediaApplication.Data.Context;
using SocialMediaApplication.Common.Exceptions;

namespace SocialMediaApplication.Data.Repositories
{
    public abstract class RepositoryBase<TEntity> where TEntity : class
    {
        internal ADataContext Context;
        internal DbSet<TEntity> DbSet;
        protected Expression<Func<TEntity, object>> PkFieldExpression;

        protected RepositoryBase(ADataContext context)
        {
            Context = context;
            DbSet = context.Set<TEntity>();
        }

        /// <summary>
        /// Gets an expression for the Primary key based on the TEntity type
        /// </summary>
        /// <param name="pk"></param>
        /// <returns></returns>
        protected Expression<Func<TEntity, bool>> FilterByPk(object pk)
        {
            var entity = Expression.Parameter(typeof (TEntity), "ent");

            Expression keyValue = Expression.Invoke(PkFieldExpression, entity);
            Expression pkValue = Expression.Constant(pk, keyValue.Type);
            Expression body = Expression.Equal(keyValue, pkValue);

            return Expression.Lambda<Func<TEntity, bool>>(body, entity);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual List<string> GetMetaData()
        {
            var schema = new List<string>();
            var entity = typeof (TEntity);

            foreach (var p in entity.GetProperties())
            {
                schema.Add(p.Name);
            }

            return schema;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public virtual List<TEntity> SqlQueryList(string query, params object[] parameters)
        {
            return DbSet.SqlQuery(query, parameters).ToList<TEntity>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public virtual TEntity SqlQueryFirst(string query, params object[] parameters)
        {
            return DbSet.SqlQuery(query, parameters).FirstOrDefault<TEntity>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public virtual TEntity SqlQuerySingle(string query, params object[] parameters)
        {
            return DbSet.SqlQuery(query, parameters).SingleOrDefault<TEntity>();
        }

        /// <summary>
        ///   Gets the object(s) is exists in database by specified filter.
        /// </summary>
        /// <param name="predicate"> Specified the filter expression </param>
        public virtual bool Exists(Expression<Func<TEntity, bool>> predicate)
        {
            return DbSet.Any<TEntity>(predicate);
        }

        /// <summary>
        ///   Get the total objects count.
        /// </summary>
        public int Count()
        {
            return DbSet.Count<TEntity>();
        }

        /// <summary>
        ///   Gets the number of objects in database with specified filter.
        /// </summary>
        /// <param name="predicate"> Specified the filter expression </param>
        public int Count(Expression<Func<TEntity, bool>> predicate)
        {
            return DbSet.Count<TEntity>(predicate);
        }

        /// <summary>
        ///   Gets all objects from database
        /// </summary>
        public virtual List<TEntity> GetAll()
        {
            return DbSet.ToList<TEntity>();
        }

        /// <summary>
        ///   Gets object by primary key.
        /// </summary>
        /// <param name="id"> primary key </param>
        /// <returns> </returns>
        public virtual TEntity GetById(object id)
        {
            return DbSet.Find(id);
        }

        /// <summary>
        ///   Find object by keys.
        /// </summary>
        /// <param name="keys"> Specified the search keys. </param>
        public virtual TEntity GetById(params object[] keys)
        {
            return DbSet.Find(keys);
        }

        /// <summary>
        ///   Find object by specified expression.
        /// </summary>
        /// <param name="predicate"> </param>
        public virtual TEntity Get(Expression<Func<TEntity, bool>> predicate)
        {
            return DbSet.FirstOrDefault<TEntity>(predicate);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual IQueryable<TEntity> GetAllAsQueryable()
        {
            return DbSet.AsQueryable<TEntity>();
        }

        /// <summary>
        ///   Gets objects from database by filter.
        /// </summary>
        /// <param name="predicate"> Specified a filter </param>
        public virtual IQueryable<TEntity> GetAsQueryable(Expression<Func<TEntity, bool>> predicate)
        {
            return DbSet.Where(predicate).AsQueryable<TEntity>();
        }

        /// <summary>
        /// Gets objects via optional filter, sort order, and includes
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="orderBy"></param>
        /// <param name="includeProperties"></param>
        /// <returns></returns>
        public virtual IQueryable<TEntity> GetAsQueryable(Expression<Func<TEntity, bool>> predicate = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, string includeProperties = "")
        {
            IQueryable<TEntity> query = DbSet;

            if (predicate != null)
            {
                query = query.Where(predicate);
            }

            if (!string.IsNullOrWhiteSpace(includeProperties))
            {
                foreach (var includeProperty in includeProperties.Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(includeProperty);
                }
            }

            return orderBy != null ? orderBy(query).AsQueryable() : query.AsQueryable();
        }

        /// <summary>
        /// Gets objects from database with filting and paging.
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="total"></param>
        /// <param name="index"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public virtual IQueryable<TEntity> GetAsQueryable(Expression<Func<TEntity, bool>> predicate, out int total, int index = 0, int size = 50)
        {
            var skipCount = index*size;

            var resetSet = predicate != null ? DbSet.Where(predicate).AsQueryable<TEntity>() : DbSet.AsQueryable<TEntity>();
            resetSet = skipCount == 0 ? resetSet.Take(size) : resetSet.Skip(skipCount).Take(size);
            total = resetSet.Count();

            return resetSet.AsQueryable();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        public virtual void Insert(TEntity entity)
        {
            DbSet.Add(entity);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        public virtual void Update(TEntity entity)
        {
            DbSet.Attach(entity);
            Context.Entry(entity).State = EntityState.Modified;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="id"></param>
        /// <param name="updateStrategy"></param>
        /// <returns></returns>
        public bool UpdateByIdFromObject(object obj, long id, Action<TEntity, object> updateStrategy)
        {
            try
            {
                if (obj == null)
                    throw new DatabaseUpdateException($"Unable to Update Entity: '{typeof (TEntity)}' from a null Object");

                if (id <= 0)
                    throw new DatabaseUpdateException($"Unable to Update Entity: '{typeof (TEntity)}' the Id: {id} is not valid");

                var dbEntity = DbSet.Find(id);

                //Check if an entity was found and throw an exeption if not
                if (dbEntity == null)
                {
                    throw new DatabaseUpdateException($"Unable to Update Entity: '{typeof (TEntity)}' no entity was found with Id: {id}");
                }

                updateStrategy(dbEntity, obj);

                return true;
            }
            catch (Exception ex)
            {
                throw new DatabaseUpdateException($"Entity: '{typeof (TEntity)}' could not be updated", ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="id"></param>
        /// <param name="updateStrategy"></param>
        /// <returns></returns>
        public bool UpdateByIdFromObject(object obj, int id, Action<TEntity, object> updateStrategy)
        {
            try
            {
                if (obj == null)
                    throw new DatabaseUpdateException($"Unable to Update Entity: '{typeof (TEntity)}' from a null Object");

                if (id <= 0)
                    throw new DatabaseUpdateException($"Unable to Update Entity: '{typeof (TEntity)}' the Id: {id} is not valid");

                var dbEntity = DbSet.Find(id);

                //Check if an entity was found and throw an exeption if not
                if (dbEntity == null)
                {
                    throw new DatabaseUpdateException($"Unable to Update Entity: '{typeof (TEntity)}' no entity was found with Id: {id}");                     
                }

                updateStrategy(dbEntity, obj);

                return true;
            }
            catch (Exception ex)
            {
                throw new DatabaseUpdateException($"Entity: '{typeof (TEntity)}' could not be updated", ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        public virtual void Delete(TEntity entity)
        {
            if (Context.Entry(entity).State == EntityState.Detached)
            {
                DbSet.Attach(entity);
            }

            DbSet.Remove(entity);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        public virtual void Delete(object id)
        {
            var entity = DbSet.Find(id);

            if (entity != null)
            {
                Delete(entity);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="predicate"></param>
        public virtual void Delete(Expression<Func<TEntity, bool>> predicate)
        {
            var entitiesToDelete = GetAsQueryable(predicate);

            foreach (var entity in entitiesToDelete)
            {
                if (Context.Entry(entity).State == EntityState.Detached)
                {
                    DbSet.Attach(entity);
                }

                DbSet.Remove(entity);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public EntityState GetEntityState(TEntity entity)
        {
            return Context.Entry(entity).State;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            if (Context == null)
                return;

            Context.Dispose();
            Context = null;
        }
    }
}