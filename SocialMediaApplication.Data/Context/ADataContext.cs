﻿using System.Collections.Generic;
using System.Data.Entity;
using SocialMediaApplication.Data.Models;

namespace SocialMediaApplication.Data.Context
{
    public abstract class ADataContext : DbContext
    {
        protected ADataContext(string dataConnection) : base(dataConnection)
        {
        }

        public abstract DbSet<TwitterAccess> TwitterAccesses { get; set; }
        public abstract string ServerName { get; }
        public abstract string DatabaseName { get; }
        public abstract int? CommandTimeout { get; }
        public abstract void InitContext(int? commandTimeout);
        public abstract List<T> SqlQueryList<T>(string query, params object[] parameters);
        public abstract T SqlQueryFirst<T>(string query, params object[] parameters);
        public abstract T SqlQuerySingle<T>(string query, params object[] parameters);
        public abstract int SqlCommand(string query, params object[] parameters);
    }
}