﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using SocialMediaApplication.Data.Models;

namespace SocialMediaApplication.Data.Context
{
    public class DataContext : ADataContext
    {
        public override DbSet<TwitterAccess> TwitterAccesses { get; set; }

        public DataContext() : base("DefaultConnection")
        {
            InitContext(null);
        }

        //Passing a null timeout value means that the default timeout specified in the Init method is used
        public  DataContext(int? commandTimeout) : base("DefaultConnection")
        {
            InitContext(commandTimeout);
        }

        public sealed override void InitContext(int? commandTimeout)
        {
            //Set the CommandTimeout to 180 seconds
            //This can also be set in the connection string by using ;Default Command Timeout=180
            //However this will override the connection string value
            if (commandTimeout.HasValue)
            {
                Database.CommandTimeout = commandTimeout.Value;
            }
            else
            {
                Database.CommandTimeout = 180; //Use 180 seconds as the new default rather than framework default of 30 seconds
            }

            //To trace the SQL and see the SQL queries that EF generates
            Database.Log = s => Debug.WriteLine(s);
        }

        public override string ServerName
        {
            get { return Database.Connection.DataSource; }
        }

        public override string DatabaseName
        {
            get { return Database.Connection.Database; }
        }

        public override int? CommandTimeout
        {
            get { return Database.CommandTimeout; }
        }

        public override List<T> SqlQueryList<T>(string query, params object[] parameters)
        {
            return Database.SqlQuery<T>(query, parameters).ToList<T>();
        }

        public override T SqlQueryFirst<T>(string query, params object[] parameters)
        {
            return Database.SqlQuery<T>(query, parameters).FirstOrDefault<T>();
        }

        public override T SqlQuerySingle<T>(string query, params object[] parameters)
        {
            return Database.SqlQuery<T>(query, parameters).SingleOrDefault<T>();
        }

        public override int SqlCommand(string query, params object[] parameters)
        {
            return Database.ExecuteSqlCommand(query, parameters);
        }
    }
}