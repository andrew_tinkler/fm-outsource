﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using SocialMediaApplication.Models.Authorisation;
using SocialMediaApplication.Models.Data;

[assembly: InternalsVisibleTo("SocialMediaApplication.Tests")]
namespace SocialMediaApplication.Business.SocialMediaStrategies
{
    public abstract class ASocialConnection
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public abstract ADataModel Execute(params string[] searchStrings);

        /// <summary>
        /// 
        /// </summary>
        ///  <param name="count"></param>
        /// <param name="searchStrings"></param>
        /// <returns></returns>
        public abstract List<ADataModel> Execute(int count, params string[] searchStrings);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="consumerPublic"></param>
        /// <param name="consumerSecret"></param>
        /// <returns></returns>
        internal abstract AAuthorisationModel GetAuthorisation(string consumerPublic, string consumerSecret);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="aAuthorisationInformation"></param>
        /// <param name="endpoint"></param>
        /// <returns></returns>
        internal abstract ADataModel CallSocialNetwork(AAuthorisationModel aAuthorisationInformation, Uri endpoint);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="aAuthorisationInformation"></param>
        /// <param name="endpoint"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        internal abstract List<ADataModel> CallSocialNetwork(AAuthorisationModel aAuthorisationInformation, Uri endpoint, int count);
    }
}
