﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using SocialMediaApplication.Common.Extensions;
using SocialMediaApplication.Common.HttpClient;
using SocialMediaApplication.Common.Serialiser;
using SocialMediaApplication.Common.SystemConfig;
using SocialMediaApplication.Data.Interfaces;
using SocialMediaApplication.Data.Models;
using SocialMediaApplication.Models.Authorisation;
using SocialMediaApplication.Models.Data;

namespace SocialMediaApplication.Business.SocialMediaStrategies
{
    public class Twitter : ASocialConnection
    {
        private readonly ISerializer _serialiser;
        private readonly IDbRepository _dbRepository;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serialiser"></param>
        /// <param name="dbRepository"></param>
        public Twitter(ISerializer serialiser, IDbRepository dbRepository)
        {
            _serialiser = serialiser;
            _dbRepository = dbRepository;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override ADataModel Execute(params string[] searchStrings)
        {
            const int count = 1;
            var twitterTimelineEndpoint = string.Format(Settings.TwitterTimelineEndpoint, count, searchStrings.FirstOrDefault());
            string consumerPublic = Settings.TwitterConsumerKey, consumerSecret = Settings.TwitterConsumerSecret;

            var authorisationModel = GetAuthorisation(consumerPublic, consumerSecret);
            var results = CallSocialNetwork(authorisationModel, twitterTimelineEndpoint.ToUri());

            AddTwitterAccess(searchStrings, count);

            return results;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="count"></param>
        /// <param name="searchStrings"></param>
        /// <returns></returns>
        public override List<ADataModel> Execute(int count, params string[] searchStrings)
        {
            var twitterTimelineEndpoint = string.Format(Settings.TwitterTimelineEndpoint, count, searchStrings.FirstOrDefault());
            string consumerPublic = Settings.TwitterConsumerKey, consumerSecret = Settings.TwitterConsumerSecret;

            var authorisationModel = GetAuthorisation(consumerPublic, consumerSecret);
            var results = CallSocialNetwork(authorisationModel, twitterTimelineEndpoint.ToUri(), count);


            AddTwitterAccess(searchStrings, count);

            return results;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="searchStrings"></param>
        /// <param name="defaultCount"></param>
        private void AddTwitterAccess(IEnumerable<string> searchStrings, int defaultCount)
        {
            _dbRepository.TwitterAccessRepository.Insert(new TwitterAccess
            {
                Count = defaultCount,
                ScreenName = searchStrings.FirstOrDefault(),
                DateCreated = DateTime.Now,
            });

            _dbRepository.Commit();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="consumerPublic"></param>
        /// <param name="consumerSecret"></param>
        /// <returns></returns>
        internal override AAuthorisationModel GetAuthorisation(string consumerPublic, string consumerSecret)
        {

            var twitterAuthorisationEndpoint = Settings.TwitterAuthorisationEndpoint;
            var basicAuthentication = $"{consumerPublic}:{consumerSecret}".Base64Encode();

            var webHeaderCollection = new WebHeaderCollection
            {
                {"Content-Type", "application/x-www-form-urlencoded;charset=UTF-8"},
                {"Authorization", $"Basic {basicAuthentication}"}
            };
            var callWebApi = new CallWebApi(webHeaderCollection);
            var result = callWebApi.SendToWebApiPost(twitterAuthorisationEndpoint.ToUri(), "grant_type=client_credentials");

            var authorisationModel = _serialiser.Deserialize<TwitterAuthorisationModel>(result);
            
            return authorisationModel;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="authorisationInformation"></param>
        /// <param name="endpoint"></param>
        /// <returns></returns>
        internal override ADataModel CallSocialNetwork(AAuthorisationModel authorisationInformation, Uri endpoint)
        {
            var twitterAuthorisationModel = (TwitterAuthorisationModel) authorisationInformation;

            var webHeaderCollection = new WebHeaderCollection
            {
                {"Accept-Encoding", "gzip"},
                {"Authorization", $"Bearer {twitterAuthorisationModel.access_token}"}
            };

            var callWebApi = new CallWebApi(webHeaderCollection);
            var result = callWebApi.SendToWebApiGet(endpoint);

            var twitterModel = _serialiser.Deserialize<TwitterDataModel>(result);

            return twitterModel;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="authorisationInformation"></param>
        /// <param name="endpoint"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        internal override List<ADataModel> CallSocialNetwork(AAuthorisationModel authorisationInformation, Uri endpoint, int count)
        {
            var twitterAuthorisationModel = (TwitterAuthorisationModel)authorisationInformation;

            var webHeaderCollection = new WebHeaderCollection
            {
                {"Accept-Encoding", "gzip"},
                {"Authorization", $"Bearer {twitterAuthorisationModel.access_token}"}
            };

            var callWebApi = new CallWebApi(webHeaderCollection);
            var result = callWebApi.SendToWebApiGet(endpoint);


            var twitterModels = _serialiser.Deserialize<List<TwitterDataModel>>(result);

            return twitterModels.Cast<ADataModel>().ToList();
        }

        
    }
}