﻿using System;
using System.Collections.Generic;
using SocialMediaApplication.Models.Authorisation;
using SocialMediaApplication.Models.Data;

namespace SocialMediaApplication.Business.SocialMediaStrategies
{
    public class NullStrategy : ASocialConnection
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override ADataModel Execute(params string[] searchStrings)
        {            
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        public override List<ADataModel> Execute(int count, params string[] searchStrings)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="consumerPublic"></param>
        /// <param name="consumerSecret"></param>
        /// <returns></returns>
        internal override AAuthorisationModel GetAuthorisation(string consumerPublic, string consumerSecret)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="aAuthorisationInformation"></param>
        /// <param name="endpoint"></param>
        /// <returns></returns>
        internal override ADataModel CallSocialNetwork(AAuthorisationModel aAuthorisationInformation, Uri endpoint)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="aAuthorisationInformation"></param>
        /// <param name="endpoint"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        internal override List<ADataModel> CallSocialNetwork(AAuthorisationModel aAuthorisationInformation, Uri endpoint, int count)
        {
            throw new NotImplementedException();
        }
    }
}
