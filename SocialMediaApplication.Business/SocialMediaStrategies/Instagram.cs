﻿
using System;
using System.Collections.Generic;
using SocialMediaApplication.Common.Serialiser;
using SocialMediaApplication.Models.Authorisation;
using SocialMediaApplication.Models.Data;
using SocialMediaApplication.Data.Interfaces;

namespace SocialMediaApplication.Business.SocialMediaStrategies
{
    public class Instagram : ASocialConnection
    {
        private readonly ISerializer _serialiser;
        private readonly IDbRepository _dbRepository;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serialiser"></param>
        /// <param name="dbRepository"></param>
        public Instagram(ISerializer serialiser, IDbRepository dbRepository)
        {
            _serialiser = serialiser;
            _dbRepository = dbRepository;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override ADataModel Execute(params string[] searchStrings)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="count"></param>
        /// <param name="searchStrings"></param>
        /// <returns></returns>
        public override List<ADataModel> Execute(int count, params string[] searchStrings)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="consumerPublic"></param>
        /// <param name="consumerSecret"></param>
        /// <returns></returns>
        internal override AAuthorisationModel GetAuthorisation(string consumerPublic, string consumerSecret)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="aAuthorisationInformation"></param>
        /// <param name="endpoint"></param>
        /// <returns></returns>
        internal override ADataModel CallSocialNetwork(AAuthorisationModel aAuthorisationInformation, Uri endpoint)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="aAuthorisationInformation"></param>
        /// <param name="endpoint"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        internal override List<ADataModel> CallSocialNetwork(AAuthorisationModel aAuthorisationInformation, Uri endpoint, int count)
        {
            throw new NotImplementedException();
        }
    }
}
