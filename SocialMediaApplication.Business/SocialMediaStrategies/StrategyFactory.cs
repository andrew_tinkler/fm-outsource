﻿namespace SocialMediaApplication.Business.SocialMediaStrategies
{
    /// <summary>
    /// 
    /// </summary>
    public class xxStrategyFactoryxx
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strategyType"></param>
        /// <returns></returns>
        public static ASocialConnection GetStrategy(StrategyTypes strategyType)
        {
            // StrategyLoader defines which message managers can be accessed
            var allowedStrategies = new StrategyLoader().GetStrategies();

            ASocialConnection socialConnection;
            var success = allowedStrategies.TryGetValue(strategyType, out socialConnection);

            return success ? socialConnection : new NullStrategy();
        }
    }
}
