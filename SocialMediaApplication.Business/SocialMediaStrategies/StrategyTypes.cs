﻿using System.ComponentModel;

namespace SocialMediaApplication.Business.SocialMediaStrategies
{
    public enum StrategyTypes
    {
        [Description("Facebook")]
        Facebook,
        [Description("Twitter")]
        Twitter,
        [Description("Instagram")]
        Instagram,
    }
}
