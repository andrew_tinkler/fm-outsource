﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using SocialMediaApplication.Common.SystemConfig;

[assembly: InternalsVisibleTo("Tests")]
namespace SocialMediaApplication.Business.SocialMediaStrategies
{
    public class StrategyLoader
    {
        private const string SectionName = "strategySettings";
        private Dictionary<StrategyTypes, ASocialConnection> _strategies;

        /// <summary>
        /// Gets the allowed strategies for handling push notifications from the Web.Config - Section strategySettings
        /// Attempts to map the Key to the TargetDevice 
        /// Then creates an instance of the class based on the Value
        /// </summary>
        /// <returns></returns>
        public Dictionary<StrategyTypes, ASocialConnection> GetStrategies()
        {
            _strategies = new Dictionary<StrategyTypes, ASocialConnection>();

            var settings = Settings.SocialMediaStrategies;

            if (settings == null)
            {
                throw new Exception($"Unable to locate Config Section for name {SectionName}");
            }

            // For each key in the Section set agains the TargetDevice Enum, then create an instance of the class defined in Value (must inherit AMessageManager)
            foreach (var setting in settings.Keys)
            {
                StrategyTypes strategyType;
                var success = Enum.TryParse(setting.ToString(), out strategyType);

                if (!success)
                    continue;

                var value = settings[setting];

                var strategyManager = CreateInstance(value.ToString());

                _strategies.Add(strategyType, strategyManager);
            }

            return _strategies;
        }

        /// <summary>
        /// Made internal to allow for testing - see Namespace Attribute above
        /// Create an instance of class based on string value - otherwise return a NullMessageManager
        /// </summary>
        /// <param name="className"></param>
        /// <returns></returns>
        internal ASocialConnection CreateInstance(string className)
        {
            try
            {
                var strategyManager = Activator.CreateInstance(GetType().Assembly.FullName, className);
                return strategyManager.Unwrap() as ASocialConnection;
            }
            catch (Exception)
            {
                return new NullStrategy();
            }
        }
    }
}